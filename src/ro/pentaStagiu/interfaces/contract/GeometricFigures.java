package ro.pentaStagiu.interfaces.contract;

public interface GeometricFigures {
	public double area(); 
	public double perimeter();
}

package ro.pentaStagiu.interfaces.implement;

import ro.pentaStagiu.interfaces.contract.GeometricFigures;

public class Circle implements GeometricFigures {
	private double radius;
	
	public Circle(){}
	
	public Circle(double radius){
		this.radius=radius;
	}
	
	public void setRadius(double radius){
		this.radius=radius;
	}
	
	public double getRadius(){
		return this.radius;
	}
	
	@Override
	public double area(){
		double area=Math.PI*this.radius*this.radius;
		return area;
	}

	@Override
	public double perimeter(){
		double perimeter=2*Math.PI*this.radius;
		return perimeter;
	}
	
	public void printResults(){
		System.out.println("============Circle============");
		System.out.println("Area: "+this.area());
		System.out.println("Perimeter: "+this.perimeter());
		System.out.println("=================================");
	}
}

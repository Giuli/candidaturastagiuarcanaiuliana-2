package ro.pentaStagiu.interfaces.implement;
import ro.pentaStagiu.interfaces.contract.GeometricFigures;

public class Rectangle implements GeometricFigures{
	private double length;
	private double width;
	
	public Rectangle(){}
	
	public Rectangle(double l,double w){
		this.length=l;
		this.width=w;
	}
	
	public void setLength(double l){
		this.length=l;
	}
	
	public void setWidth(double w){
		this.width=w;
	}
	
	public double getLength(){
		return this.length;
	}
	
	public double getWidtth(){
		return this.width;
	}
	
	@Override
	public double perimeter(){
		double perimeter=2*(this.length+this.width);
		return perimeter;
	}
	
	@Override
	public double area(){
		double area=this.length*this.width;
		return area;
	}

	public void printResults(){
		System.out.println("============Rectangle============");
		System.out.println("Area: "+this.area());
		System.out.println("Perimeter: "+this.perimeter());
		System.out.println("=================================");
	}
}

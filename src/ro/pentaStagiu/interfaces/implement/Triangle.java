package ro.pentaStagiu.interfaces.implement;
import ro.pentaStagiu.interfaces.contract.GeometricFigures;

public class Triangle implements GeometricFigures{
	private double sideA;
	private double sideB;
	private double sideC;
	
	public Triangle(){}
	
	public Triangle(double s1,double s2,double s3){
		this.sideA=s1;
		this.sideB=s2;
		this.sideC=s3;	
	}
	
	public void setSideA(double side){
		this.sideA=side;
	}
	
	public void setSideB(double side){
		this.sideB=side;
	}
	
	public void setSideC(double side){
		this.sideC=side;
	}
	
	public double getSideA(){
		return this.sideA;
	}
	
	public double getSideB(){
		return this.sideB;
	}
	
	public double getSideC(){
		return this.sideC;
	}
	
	@Override
	public double perimeter(){
		double perimeter=this.sideA+this.sideB+this.sideC;
		return perimeter;
	}
	
	@Override
	public double area(){
		double sP=this.perimeter()/2;
		double area=Math.sqrt(sP*(sP-this.sideA)*(sP-this.sideB)*(sP-this.sideC));
		return area;
	}

	public void printResults(){
		System.out.println("============Triangle============");
		System.out.println("Area: "+this.area());
		System.out.println("Perimeter: "+this.perimeter());
		System.out.println("=================================");
	}
}

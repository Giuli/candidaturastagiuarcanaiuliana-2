package ro.pentaStagiu.main;

import java.util.Scanner;

import ro.pentaStagiu.interfaces.implement.Circle;
import ro.pentaStagiu.interfaces.implement.Rectangle;
import ro.pentaStagiu.interfaces.implement.Triangle;

public class Main {

	public static void main(String[] args) {

		Scanner input=new Scanner(System.in);
		
		int ok=1;
		while(ok==1){
			System.out.println("\nPlease insert a number to select the geometric figure \nfor witch to calculate the area and the perimeter:");
			System.out.println("\t 1 for CIRCLE");
			System.out.println("\t 2 for TRIANGLE");
			System.out.println("\t 3 for RECTANGLE");

			int choice=input.nextInt();
			switch(choice)
			{
			case 1:
				double radius;
				System.out.print("Insert a value for radius: ");
				radius=input.nextFloat();
				Circle c=new Circle(radius);
				c.printResults();
				break;
			case 2:
				double s1,s2,s3;
				s1=s2=s3=0;
				int Ok=0;
				while(Ok==0)
				{
					System.out.print("Insert a value for side A of the triangle: ");
					s1=input.nextFloat();
					System.out.print("Insert a value for side B of the triangle: ");
					s2=input.nextFloat();
					System.out.print("Insert a value for side C of the triangle: ");
					s3=input.nextFloat();
					if(s1==0 || s2==0 || s3==0 || (s1+s2)<=s3 || (s1+s3)<=s2 || (s2+s3)<=s1)
						System.out.println("\nThe three sides are not a triangle! Please insert another values: ");
					else
						Ok=1;
				}
				Triangle t=new Triangle(s1,s2,s3);
				t.printResults();
				break;

			case 3:
				double length,width;
				System.out.print("Insert the length of the rectangle: ");
				length=input.nextFloat();
				System.out.print("Insert the width of the rectangle: ");
				width=input.nextFloat();
				Rectangle r=new Rectangle(length,width);
				r.printResults();
				break;
				default:
					System.out.println("Invalid option!");
					break;
			}
			
			System.out.print("\nWant to choose another geometric figure?\n" );
			System.out.print("\tPress 1 for YES\tor 0 for NO: " );
			ok=input.nextInt();
		}
		input.close();
	}

}
